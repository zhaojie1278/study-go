package main

import "fmt"

func main() {
	// Go 语言变量
	var a string = "Runnoob"
	fmt.Println(a)
	var b, c int = 1, 2
	fmt.Println(c, c, b)

	// 声明 declaration
	var a_2 = "Runoob"
	fmt.Println(a_2)

	var v_name int
	fmt.Println(v_name)

	var v_bool bool
	fmt.Println(v_bool)

	// // 其他类型
	var a2 *int
	fmt.Println(a2)
	var a3 []int
	fmt.Println(a3)
	// var a2 map[string]int
	// var a2 chan int
	// var a2 func(string) int
	// var a2 error

	// 声明
	var a4 = 16

	fmt.Println(a4)

}
